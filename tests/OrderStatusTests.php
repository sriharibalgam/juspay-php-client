<?php

require_once 'PHPUnit/Autoload.php';
require_once 'TestHelper.php';

class Juspay_OrderStatusTests extends PHPUnit_Framework_TestCase
{
    function testOrderStatusNewCharged()
    {
        $service = "/order_status";
        $params = array('order_id' => '1358513712');
        $order_status_response = json_decode(Juspay_Service::makeServiceCall($service,$params));

        $this->assertEquals($order_status_response->status,"CHARGED");
        $this->assertNotNull($order_status_response->txn_id);
    }

    function testOrderStatusNew()
    {
        #Create a new order
        $order_id = rand();
        $service = "/init_order";
        $params = array('customer_id' => 'guest_user_101' , 'customer_email' => 'customer@mail.com' , 
                             'amount' => '10.00' , 'order_id' => $order_id );
        Juspay_Service::makeServiceCall($service,$params);

        #Check the response of the newly created order
        $service = "/order_status";
        $params = array('order_id' => $order_id);
        $order_status_response = json_decode(Juspay_Service::makeServiceCall($service,$params));

        $this->assertEquals($order_status_response->status,"NEW");
        $this->assertEquals($order_status_response->merchant_id,"guest");
    }

    function testOrderStatusPendingVBV()
    {
        $service = "/order_status";
        $params = array('order_id' => '1358507824' );

        $order_status_response = json_decode(Juspay_Service::makeServiceCall($service,$params));

        $this->assertEquals($order_status_response->status,"PENDING_VBV");
    }
}

?>