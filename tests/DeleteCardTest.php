<?php

require_once 'PHPUnit/Autoload.php';
require_once 'TestHelper.php';

class Juspay_DeleteCardTest extends PHPUnit_Framework_TestCase
{
    function testDeleteCard()
    {
        $service = "/card/delete";
        #Example card_token value. 
        $params = array('card_token' => '0784ba0c-dc07-489e-a57d-16dfcb39868f');

        $delete_card_response = json_decode(Juspay_Service::makeServiceCall($service,$params));

        $this->assertEquals($delete_card_response->card_token,"0784ba0c-dc07-489e-a57d-16dfcb39868f");
        $this->assertEquals($delete_card_response->deleted,true);
    }
}

?>