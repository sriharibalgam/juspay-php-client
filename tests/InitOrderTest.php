<?php

require_once 'PHPUnit/Autoload.php';
require_once 'TestHelper.php';

class Juspay_InitOrderTest extends PHPUnit_Framework_TestCase
{
    function testInitOrderCall()
    {
        $order_id = rand();
        $service = "/init_order";
        $params = array('customer_id' => 'guest_user_101' , 'customer_email' => 'customer@mail.com' , 
                             'amount' => '10.00' , 'order_id' => $order_id );
        $init_order_response = json_decode(Juspay_Service::makeServiceCall($service,$params));

        $this->assertEquals($init_order_response->status,"CREATED");
        $this->assertEquals($init_order_response->order_id,$order_id);
    }
}

?>