<?php

require_once 'PHPUnit/Autoload.php';
require_once 'TestHelper.php';

class Juspay_AddCardTests extends PHPUnit_Framework_TestCase
{
    function testAddCard()
    {
        $service = "/card/add";
        $params = array('customer_id' => 'guest_user_101' , 'customer_email' => 'customer@mail.com' , 
                             'card_number' => '4111111111111111' , 'card_exp_year' => '2015' ,
                             'card_exp_month' =>  '07' , 'name_on_card' => 'Sindbad' );

        $add_card_response = json_decode(Juspay_Service::makeServiceCall($service,$params));

        $this->assertNotNull($add_card_response->card_token);
    }
}

?>