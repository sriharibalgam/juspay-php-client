<?php

#Helper class that sets up the testing environment.

require_once '../lib/Configuration.php';
require_once '../lib/Service.php';

function juspayTestEnvironmentConfig()
{
    #Set the environment , merchant id and Key.
    #NOTE: This is a test key and don't work in production.
    Juspay_configuration::environment("production");
    Juspay_configuration::merchantId("juspay");
    Juspay_configuration::key("782CB4B3F5B84BDDB3C9EAFA6A134DC3:");
}

juspayTestEnvironmentConfig();

?>