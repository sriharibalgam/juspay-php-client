<?php

require_once 'PHPUnit/Autoload.php';
require_once 'TestHelper.php';

class Juspay_ListCardTest extends PHPUnit_Framework_TestCase
{
    function testListCards()
    {
        $service = "/card/list";
        $params = array('customer_id' => 'guest_user_101');
        $list_card_response = json_decode(Juspay_Service::makeServiceCall($service,$params));

        $this->assertEquals($list_card_response->customer_id,"guest_user_101");
        $this->assertEquals(count($list_card_response->cards),2);
    }
}

?>