##Juspay PHP Client Library

-----------------------

The Juspay PHP library provides integration access to the Juspay services.

##Dependencies

---------------------------

PHP (>=  5.3.10) and curl required.

##Quick Start Example

-----------------------------

    <?php

    require_once 'Configuration.php';
    require_once 'Service.php';

    Juspay_configuration::environment("production");
    Juspay_configuration::merchantId("your_id");
    Juspay_configuration::key("your_key");

    //A init order example calls
    $service = "/init_order";
    $params = array('customer_id' => 'guest_user_101' , 'customer_email' => 'customer@mail.com' , 
                                 'amount' => '10.00' , 'order_id' => rand() );
    $init_order_result = json_decode(Juspay_Service::makeServiceCall($service,$params));

    if($init_order_result->status == "CREATED")
        print_r("Order created successfully\n");

    ?>

More examples can be found in `Example.php` located under `lib/` directory.

##Running The Test Cases.

-------------------------------------------

Make sure you have installed `phpunit` on your machine. If you don't have it please follow these steps:

    sudo apt-get remove phpunit

    sudo pear channel-discover pear.phpunit.de

    sudo pear channel-discover pear.symfony-project.com

    sudo pear channel-discover components.ez.no

    sudo pear update-channels

    sudo pear upgrade-all

    sudo pear install --alldeps phpunit/PHPUnit

    sudo pear install --force --alldeps phpunit/PHPUnit

Once the `phpunit` have been installed you can run our test suite as:

    phpunit . //under $DIR/tests directory


For queries contact itadmin@juspay.in.
