<?php

require_once 'Configuration.php';
require_once 'Service.php';

Juspay_configuration::environment("production");
Juspay_configuration::merchantId("juspay");
Juspay_configuration::key("782CB4B3F5B84BDDB3C9EAFA6A134DC3:");

//A init order example calls
$service = "/init_order";
$params = array('customer_id' => 'guest_user_101' , 'customer_email' => 'customer@mail.com' , 
                             'amount' => '10.00' , 'order_id' => rand() );
$init_order_result = json_decode(Juspay_Service::makeServiceCall($service,$params));

if($init_order_result->status == "CREATED")
    print_r("Order created successfully\n");

?>