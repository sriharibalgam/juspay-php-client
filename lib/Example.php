<?php

require_once 'Configuration.php';
require_once 'Service.php';

#Set the environment , merchant id and Key.
#NOTE: This is a test key and don't work in production.
Juspay_configuration::environment("production");
Juspay_configuration::merchantId("php_client_user_101");
Juspay_configuration::key("568C066211D84AA8AA006982214B06F3:");


#NOTE: The order_id are the fields associated with the "current" order.
$order_id = rand();

echo "Example Response for Init Order\n\n";
$service = "/init_order";
#Set the required params for init order call
$params = array('customer_id' => 'guest_user_101' , 'customer_email' => 'customer@mail.com' , 
                             'amount' => '10.00' , 'order_id' => $order_id );
echo Juspay_Service::makeServiceCall($service,$params);

echo "\n\n";

echo "Example Response for Init Order with return url\n";
$service = "/init_order";
$new_order_id = rand();
$params = array('customer_id' => 'guest_user_101' , 'customer_email' => 'customer@mail.com' , 
                             'amount' => '10.00' , 'order_id' => $new_order_id, 'return_url' => "http://merchant.com/payment_status.php");
echo Juspay_Service::makeServiceCall($service,$params);

echo "\n\n";

echo "Example Response for Add Card\n";
$service = "/card/add";
$params = array('customer_id' => 'guest_user_101' , 'customer_email' => 'customer@mail.com' , 
                             'card_number' => '4111111111111111' , 'card_exp_year' => '2015' ,
                             'card_exp_month' =>  '07' , 'name_on_card' => 'Sindbad' );
echo Juspay_Service::makeServiceCall($service,$params);

echo "\n\n";

echo "Example Response for List  card\n";
$service = "/card/list";
$params = array('customer_id' => 'guest_user_101');
echo Juspay_Service::makeServiceCall($service,$params);

echo "\n\n";

echo "Example Response for delete card\n";
$service = "/card/delete";
#Example card_token value. 
$params = array('card_token' => '0784ba0c-dc07-489e-a57d-16dfcb39868f');
echo Juspay_Service::makeServiceCall($service,$params);

echo "\n\n";

/* ---- A bunch of order status example calls ---- */

echo  "Example Response for order status (CHARGED):\n";
$service = "/order_status";
$params = array('order_id' => '1358513712' );
echo Juspay_Service::makeServiceCall($service,$params);

echo "\n\n";

echo "Example Response for order status (NEW):\n";
$service = "/order_status";
$params = array('order_id' => $order_id );
echo Juspay_Service::makeServiceCall($service,$params);

echo "\n\n";

echo "Example Response for order status (NEW) with return Url:\n";
$service = "/order_status";
$params = array('order_id' => $new_order_id );
echo Juspay_Service::makeServiceCall($service,$params);

echo "\n\n";

echo "Example Respone for order status (PENDING_VBV):\n";
$service = "/order_status";
$params = array('order_id' => '1358507824' );
echo Juspay_Service::makeServiceCall($service,$params);

echo "\n\n";


?>
