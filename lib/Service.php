<?php

require_once "Configuration.php";

class Juspay_Service {
    

    /**
    * Returns the contents of the end-point
    * @access public
    * @static
    * @param service specifies what action need to be taken
    * @param array the list of parameters that are required to 
    *        perform the call
    * @return the response of the end-point
    */
    public static function makeServiceCall($service,Array $array)
    {
        
        $url = self::constructURL($service);

        #First initiate the order using init_order api call
        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);                
        curl_setopt($ch, CURLOPT_USERPWD, Juspay_configuration::key());
        curl_setopt($ch, CURLOPT_POST, 1); 

        curl_setopt($ch, CURLOPT_POSTFIELDS, $array );
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);                    
        curl_setopt($ch,CURLOPT_TIMEOUT, 15); 

        $response = curl_exec($ch);

        return $response;
    }

    /**
    * Constructs the url based on the configs
    * @access public
    * @static 
    * @param append_url the url that need to be appended
    * @return newly constructed url
    */
    public static function constructURL($append_url)
    {
        return Juspay_configuration::getBaseUrl() . $append_url;
    }
}

?>