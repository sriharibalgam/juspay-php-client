<?php

    require_once 'Exception.php';

    class Juspay_configuration {
        
        /**
         * @var array array of config properties
         * @access protected
         * @static
         */
        private static $_environment = array('environment' => '' ,  'merchantId' => '' , 'key' => '' );

        private $_key;
        
        /**
         *
         * @access protected
         * @static
         * @var array valid environments, used for validation
         */
        private static $_validEnvironments = array(
                            'development',
                            'sandbox',
                            'production'
                        );

        /**
         * resets configuration to default
         * @access public
         * @static
         */
        public static function reset()
        {
            self::$_environment = array('environment' => '' ,  'merchantId' => '' );
        }

        
        /**
         * performs sanity checks when config settings are being set
         *
         * @ignore
         * @access protected
         * @param string $key name of config setting
         * @param string $value value to set
         * @throws InvalidArgumentException
         * @throws Juspay_Exception_Configuration
         * @static
         * @return boolean
         */
        private static function validate($key=null,$value=null)
        {
            if(empty($key) && empty($value))
                throw new InvalidArgumentException("Please provide the environment and merchantId");

            if($key == 'environment' && !in_array($value, self::$_validEnvironments))
                throw new Juspay_Exception_Configuration($value . " is not a valid environment");

            if(!isset(self::$_environment[$key]))
                throw new Juspay_Exception_Configuration($key . "not a valid configuration setting");

            if(empty($value))
                throw new InvalidArgumentException($key . "cannot be empty");

            return true;
        }

        private static function set($key,$value)
        {
            //if it fails it will throw an exception
            self::validate($key,$value);

            self::$_environment[$key] = $value;
        }

        private static function get($key)
            {
                // throw an exception if the value hasn't been set
                if (isset(self::$_environment[$key]) &&
                   (empty(self::$_environment[$key]))) {
                    throw new Juspay_Exception_Configuration(
                              $key.' needs to be set'
                              );
                }

                if (array_key_exists($key, self::$_environment)) {
                    return self::$_environment[$key];
                }

                // return null by default to prevent __set from overloading
                return null;
            }


        private static function setOrGet($name, $value = null)
        {
            if (!empty($value) && is_array($value)) {
                $value = $value[0];
            }
            if (!empty($value)) {
                self::set($name, $value);
            } else {
                return self::get($name);
            }
            return true;
        }

        /** 
        * sets or returns the property after validation
         * @access public
         * @static
         * @param string $value pass a string to set, empty to get
         * @return mixed returns true on set
         */
        public static function environment($value = null)
        {
            return self::setOrGet(__FUNCTION__, $value);
        }

        public static function merchantId($value = null)
        {
            return self::setOrGet(__FUNCTION__, $value);
        }


        public static function key($value = null)
        {
            return self::setOrGet(__FUNCTION__,$value);
        }


        /**
         * returns the base juspay gateway URL based on config values
         *
         * @access public
         * @static
         * @param none
         * @return string juspay gateway URL
         */
        public static function getBaseUrl()
        {
            switch(self::environment()) {
             case 'production':
                 $serverName = 'https://api.juspay.in';
                 break;
             case 'sandbox':
                 $serverName = 'https://sandbox.juspay.in';
                 break;
             case 'development':
             default:
                 $serverName = 'https://local.api.juspay.in';
                 break;
            }

            return $serverName;
        }
    }

?>
